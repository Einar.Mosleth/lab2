package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {
	
	int max_size = 20;	//etablerer max antall items man kan ha i kjøleskapet
	List<FridgeItem> items = new ArrayList<>();	//lager en liste med items fra FridgeItem klassen

	@Override
	public int nItemsInFridge() {	
		return items.size();	//returnerer størrelsen på lista som er antall varer i kjøleskapet
	}

	@Override
	public int totalSize() {
		return max_size;		//returnerer max antall i kjøleskapet
	}

	@Override
	public boolean placeIn(FridgeItem item) {
		// når kan man plassere i kjøleskapet og når kan man ikke?
		if (nItemsInFridge() < max_size) {	//gjør at man kan legge til flere items så lenge antallet er under max kapasiteten på 20.
			items.add(item);
			return true;
		}

		return false;	//når antallet blir 20 vil man ikke lengre kunne legge inn flere items
	}

	@Override
	public void takeOut(FridgeItem item) {
										//skal fjerne items fra kjøleskapet, remove metode?
		if(!items.contains(item)) {		//gjør at hvis kjøleskapet er tomt så kan man ikke ta ut items
			throw new NoSuchElementException();
		}
		else items.remove(item);	//fjerner items fra kjøleskapet
		
	}

	@Override
	public void emptyFridge() {	//fjerner alt i kjøleskapet
		items.clear();
	}

	@Override
	public List<FridgeItem> removeExpiredFood() {
		List<FridgeItem> removedItems = new ArrayList<>();	//lager en liste med varer som skal fjernes fordi de er utgått
		for(FridgeItem expired : items) {					//går gjennom alle varer i lista items
			if(expired.hasExpired()) {						//finner alle varer med utgått dato
				removedItems.add(expired);					//legger alle utgåtte varer i lista expired
			}
		}
		items.removeAll(removedItems);						//fjerner alle elementer fra removedItems lista
		return removedItems;								//returnerer en tom liste

	}
}